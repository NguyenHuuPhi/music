package com.example.music;

import android.app.Activity;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    @BindView(R.id.btnSelect)
    Button btnSelect;

    @BindView(R.id.btnPlay)
    Button btnPlay;

    @BindView(R.id.btnPause)
    Button btnPause;

    @BindView(R.id.tvLesson)
    TextView tvLesson;

    @BindView(R.id.tvNameMusic)
    TextView tvNameMusic;

    @BindView(R.id.tvTimeMusic)
    TextView tvTimeMusic;

    private static final int REQUEST_CODE = 100;
    public static final String TAG_MUSIC = "music";
    private String music;
    private MediaPlayer mediaPlayer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        initView();
    }

    private void initView() {
        btnSelect.setOnClickListener(this);
        btnPlay.setOnClickListener(this);
        btnPause.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnSelect:
                Intent intentSelect = new Intent(MainActivity.this, SelectSongActivity.class);
                startActivityForResult(intentSelect, REQUEST_CODE);
                if(mediaPlayer != null){
                    mediaPlayer.stop();
                }
                break;
            case R.id.btnPlay:
                if (TextUtils.isEmpty(music)) {
                    Toast.makeText(this,getResources().getString( R.string.ban_chua_chon_bai_hat), Toast.LENGTH_SHORT).show();
                } else {
                    mediaPlayer.start();
                }
                break;
            case R.id.btnPause:
                if(TextUtils.isEmpty(music)){
                    Toast.makeText(this,getResources().getString( R.string.ban_chua_chon_bai_hat), Toast.LENGTH_SHORT).show();
                }else {
                    mediaPlayer.pause();
                }

        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK && requestCode == REQUEST_CODE && data != null) {
            if (data.getStringExtra(MainActivity.TAG_MUSIC) != null) {
                music = data.getStringExtra(MainActivity.TAG_MUSIC);
            }
            setupData(music);
            tvLesson.setVisibility(View.VISIBLE);
            tvNameMusic.setVisibility(View.VISIBLE);
            tvTimeMusic.setVisibility(View.VISIBLE);
        }
    }

    private void setupData(String name) {
        if (name.equals(getResources().getString(R.string.one))) {
            tvLesson.setText(R.string.lesson_one);
            tvNameMusic.setText(R.string.ai_no);
            tvTimeMusic.setText("3 min,20 sec");
            mediaPlayer = MediaPlayer.create(this, R.raw.ai_no);

        } else if (name.equals(getResources().getString(R.string.two))) {
            tvLesson.setText(R.string.lesson_two);
            tvNameMusic.setText(R.string.cau_hua_chua_tron);
            tvTimeMusic.setText("4 min,31 sec");
            mediaPlayer = MediaPlayer.create(this, R.raw.cau_hua_chua_ven_tron);

        } else if (name.equals(getResources().getString(R.string.three))) {
            tvLesson.setText(R.string.lesson_three);
            tvNameMusic.setText(R.string.cuoi_thoi);
            tvTimeMusic.setText("3 min,02 sec");
            mediaPlayer = MediaPlayer.create(this, R.raw.cuoi_thoi);

        } else if (name.equals(getResources().getString(R.string.four))) {
            tvLesson.setText(R.string.lesson_four);
            tvNameMusic.setText(R.string.sai_gon_dau_long_qua);
            tvTimeMusic.setText("5 min,08 sec");
            mediaPlayer = MediaPlayer.create(this, R.raw.sai_gon_dau_long_qua);

        } else if (name.equals(getResources().getString(R.string.five))) {
            tvLesson.setText(R.string.lesson_five);
            tvNameMusic.setText(R.string.the_luong);
            tvTimeMusic.setText("5 min,13 sec");
            mediaPlayer = MediaPlayer.create(this, R.raw.the_luong);

        } else if (name.equals(getResources().getString(R.string.six))) {
            tvLesson.setText(R.string.lesson_six);
            tvNameMusic.setText(R.string.tinh_thuong_phu_the);
            tvTimeMusic.setText("4 min,34 sec");
            mediaPlayer = MediaPlayer.create(this, R.raw.tinh_thuong_phu_the);
        }
    }
}