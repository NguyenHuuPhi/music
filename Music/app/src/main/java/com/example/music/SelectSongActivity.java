package com.example.music;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SelectSongActivity extends AppCompatActivity implements View.OnClickListener {

    @BindView(R.id.tvName)
    TextView tvName;

    @BindView(R.id.btnOne)
    Button btnOne;

    @BindView(R.id.btnTwo)
    Button btnTwo;

    @BindView(R.id.btnThree)
    Button btnThree;

    @BindView(R.id.btnFour)
    Button btnFour;

    @BindView(R.id.btnFive)
    Button btnFive;

    @BindView(R.id.btnSix)
    Button btnSix;

    @BindView(R.id.btnChoose)
    Button btnChoose;

    String music;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_song);
        ButterKnife.bind(this);
        initlistener();

    }

    private void initlistener() {
        btnOne.setOnClickListener(this);
        btnTwo.setOnClickListener(this);
        btnThree.setOnClickListener(this);
        btnFour.setOnClickListener(this);
        btnFive.setOnClickListener(this);
        btnSix.setOnClickListener(this);
        btnChoose.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnOne:
                music = getResources().getString(R.string.one);
                tvName.setText(music);
                break;
            case R.id.btnTwo:
                music = getResources().getString(R.string.two);
                tvName.setText(music);
                break;
            case R.id.btnThree:
                music = getResources().getString(R.string.three);
                tvName.setText(music);
                break;
            case R.id.btnFour:
                music = getResources().getString(R.string.four);
                tvName.setText(music);
                break;
            case R.id.btnFive:
                music = getResources().getString(R.string.five);
                tvName.setText(music);
                break;
            case R.id.btnSix:
                music = getResources().getString(R.string.six);
                tvName.setText(music);
                break;
            case R.id.btnChoose:
                Intent intent = new Intent();
                intent.putExtra(MainActivity.TAG_MUSIC, music);
                setResult(Activity.RESULT_OK, intent);
                finish();
                break;
        }
    }
}